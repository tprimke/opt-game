var opt  = require('./opt-original'),
    flux = require('tc-flux');

var net;

var testNet = function(f) {
  f(net);
};

exports.testNet = testNet;

var createStores = function() {

  var updateCallback = (callbacks, data ) => {
    callbacks.setValue( data );
    callbacks.updated();
  };

  var setupCallback = (spec) => {
    return (callbacks, data) => {
      if ( data.hasOwnProperty('signature') ) {
        switch (data.signature) {
          case spec.setup:
            updateCallback(callbacks, data.data.token);
            break;
          
          case spec.other:
            let place = net.getPlace(spec.place).token;
            updateCallback(callbacks, place);
            break;

          default:
            throw new Error( `Unknown signature: ${data.signature}` );
        }
      }
      else
        updateCallback(callbacks, data);
    };
  };
  
  // create the A store
  flux.createStore({
    name: 'A',
    init: net.getPlace('sta').token,
    data: ['sta-raw', 'sta-setup', 'sta-update'],
    onData: setupCallback({
      setup: 'sta-setup'
    })
  });

  // create the B store
  flux.createStore({
    name: 'B',
    init: net.getPlace('stb').token,
    data: ['stb-raw', 'stb-setup', 'stb-update'],
    onData: setupCallback({
      setup: 'stb-setup'
    })
  });

  // create the C store
  flux.createStore({
    name: 'C',
    init: net.getPlace('stc').token,
    data: ['sta-setup', 'stc-update', 'stc-setup'],
    onData: setupCallback({
      setup: 'stc-setup',
      other: 'sta-setup',
      place: 'stc'
    })
  });

  // create the D store
  flux.createStore({
    name: 'D',
    init: net.getPlace('std').token,
    data: ['stb-setup', 'std-update', 'std-setup'],
    onData: setupCallback({
      setup: 'std-setup',
      other: 'stb-setup',
      place: 'std'
    })
  });

  // create the E store
  flux.createStore({
    name: 'E',
    init: net.getPlace('ste').token,
    data: ['ste-setup', 'ste-update'],
    //onData: updateCallback
    onData: setupCallback({
      setup: 'ste-setup'
    })
  });

  // create the cash store
  flux.createStore({
    name: 'cash',
    init: net.getPlace('cash').token,
    data: ['cash', 'update-cash'],
    onData: updateCallback
  });

  // create the market store
  flux.createStore({
    name: 'market',
    init: net.getPlace('market').token,
    data: ['update-market'],
    onData: updateCallback
  });  

  // create the time store
  flux.createStore({
    name: 'time',
    init: net.getTime(),
    data: 'tick',
    onData: updateCallback
  });

  // create the week/game state store
  flux.createStore({
    name: 'week',
    init: { game_over: false, week: 1 },
    data: 'update-week',
    onData: (callbacks, game_state) => {
      let val = flux.getStore('week');
      val.game_over = game_state.game_over;
      val.week += 1;
      callbacks.setValue( val );
      callbacks.updated();
    }
  });
};

var buyRawCallback = function( spec ) {
  return (callbacks, data = 1) => {
    for ( let i = 0; i < data; i++ )
      net.fire( spec.transition );
    let cash = net.getPlace('cash').token,
        place = net.getPlace(spec.place).token;
    callbacks.data('cash', cash);
    callbacks.data(spec.signature, place);
    callbacks.done();
  };
};

var createActions = function() {

  let updateGeneral = (spec) => {
    return (callbacks) => {
      let place = net.getPlace(spec.place).token;
      callbacks.data( spec.update, place );
      callbacks.done();
    };
  };

  let updateCallback = (spec) => {
    return updateGeneral({
      place:  `st${spec.station}`,
      update: `st${spec.station}-update`
    });
  };

  let createUpdateAction = (spec) => {
    flux.createAction({
      name: `update-${spec.station}`,
      onRun: updateCallback(spec)
    });
  };

  let setupCallback = (spec) => {
    return (callbacks) => {
      net.fire(`setup-${spec.station}`);
      let place = net.getPlace(`st${spec.station}`);
      callbacks.data( `st${spec.station}-setup`, {
        signature: `st${spec.station}-setup`,
        data: place
      });
      callbacks.done();
    };
  };

  let createSetupAction = (spec) => {
    flux.createAction({
      name: `setup-${spec.station}`,
      onRun: setupCallback(spec)
    });
  };

  // buy the raw material for the workstation A
  flux.createAction({
    name: 'buy-a',
    onRun: buyRawCallback({
      transition: 'buy-a',
      place:      'sta',
      signature:  'sta-raw'
    })
  });

  // buy the raw material for the workstation B
  flux.createAction({
    name: 'buy-b',
    onRun: buyRawCallback({
      transition: 'buy-b',
      place:      'stb',
      signature:  'stb-raw'
    })
  });

  // setup the workstations
  createSetupAction({ station: 'a' });
  createSetupAction({ station: 'b' });
  createSetupAction({ station: 'c' });
  createSetupAction({ station: 'd' });
  createSetupAction({ station: 'e' });
  
  // update the state of workstations
  createUpdateAction({ station: 'a' });
  createUpdateAction({ station: 'b' });
  createUpdateAction({ station: 'c' });
  createUpdateAction({ station: 'd' });
  createUpdateAction({ station: 'e' });

  let transCallback = (spec) => {
    return (callbacks, data = 1) => {
      for ( let i = 0; i < data; i++ )
        net.fire( `trans-${spec}e` );
    };
  };

  // transport actions
  flux.createAction({
    name: 'transAE',
    onRun: transCallback('a')
  });
  flux.createAction({
    name: 'transDE',
    onRun: transCallback('d')
  });

  flux.createAction({
    name: 'update-cash',
    onRun: updateGeneral({
      place:  'cash',
      update: 'update-cash'
    })
  });

  flux.createAction({
    name: 'update-market',
    onRun: updateGeneral({
      place:  'market',
      update: 'update-market'
    })
  });

  flux.createAction({
    name: 'update-week',
    onRun: (callbacks, game_state) => {
      callbacks.data( 'update-week', game_state );
      callbacks.done();
    }
  });

  let sellCallback = (spec) => {
    return (callbacks, data) => {
      for ( let i = 0; i < data; i++ )
        net.fire(`sell-${spec}`);
      let cash = net.getPlace('cash').token,
          market = net.getPlace('market').token;
      callbacks.data( 'cash', cash );
      callbacks.data( 'update-market', market );
      callbacks.done();
    };
  };

  // sell actions
  flux.createAction({
    name: 'sell-a',
    onRun: sellCallback('a')
  });
  flux.createAction({
    name: 'sell-d',
    onRun: sellCallback('d')
  });
  
  // tick
  flux.createAction({
    name: 'tick',
    onRun: (callbacks) => {
      net.tick();
      let time = net.getTime();
      callbacks.data( 'tick', time );
      callbacks.done();
    }
  });
};

var queueUpdate = function(place) {
  return () => {
    flux.queue( `update-${place}` );
  };
};

var init = function() {
  // create the net
  net = opt.createNet({
    updateCash:   queueUpdate('cash'),
    updateA:      queueUpdate('a'),
    updateB:      queueUpdate('b'),
    updateC:      queueUpdate('c'),
    updateD:      queueUpdate('d'),
    updateE:      queueUpdate('e'),
    updateMarket: queueUpdate('market'),
    afterWeek:    (game_state) => flux.queue('update-week', game_state)
  });

  flux.clear();

  // create the stores
  createStores();

  // create the actions
  createActions();
};

exports.init = init;

var onA = function( handler ) {
  flux.registerListener( handler, 'A' );
};

exports.onA = onA;

var onB = function( handler ) {
  flux.registerListener( handler, 'B' );
};

exports.onB = onB;

var onC = function( handler ) {
  flux.registerListener( handler, 'C' );
};

exports.onC = onC;

var onD = function( handler ) {
  flux.registerListener( handler, 'D' );
};

exports.onD = onD;

var onE = function( handler ) {
  flux.registerListener( handler, 'E' );
};

exports.onE = onE;

var onCash = function( handler ) {
  flux.registerListener( handler, 'cash' );
};

exports.onCash = onCash;

var onMarket = function( handler ) {
  flux.registerListener( handler, 'market' );
};

exports.onMarket = onMarket;

var onWeek = function( handler ) {
  flux.registerListener( handler, 'week' );
};

exports.onWeek = onWeek;

var buyA = function( how_many = 1 ) {
  flux.queue( 'buy-a', how_many );
};

exports.buyA = buyA;

var buyB = function( how_many = 1 ) {
  flux.queue( 'buy-b', how_many );
};

exports.buyB = buyB;

var tick = function() {
  flux.queue('tick');
};

exports.tick = tick;

var onTime = function( handler ) {
  flux.registerListener( handler, 'time' );
};

exports.onTime = onTime;

var setup = function( station ) {
  station = station.toLowerCase();
  flux.queue(`setup-${station}`);
};

exports.setup = setup;

var transAE = function( how_many = 1 ) {
  flux.queue('transAE', how_many);
};

exports.transAE = transAE;

var transDE = function( how_many = 1 ) {
  flux.queue('transDE', how_many);
};

exports.transDE = transDE;

var sell = function( part, how_many = 1 ) {
  part = part.toLowerCase();
  flux.queue(`sell-${part}`, how_many);
};

exports.sell = sell;
