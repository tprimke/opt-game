var React = require('react'),
    flux  = require('tc-flux'),
    _     = require('lodash'),
    game  = require('./game');

var ready = function (fn) {
  if (document.readyState !== 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }  
};

/*
flux.createStore({
  name: 'AppStore',

  init: {
    screen: 'title'
  },

  onData: function(data, callbacks) {
    var instore = flux.getStore('AppStore');
    if ( instore.screen === data ) {
      callbacks.notUpdated();
      return;
    }

    instore.screen = data;
    callbacks.setValue( instore );
    callbacks.updated();
  }
});

var goTitleScreen = flux.createAction({
  
  onRun: function() {

  }
});

*/
var ColMd = React.createClass({

  render: function() {
    let size = this.props.size,
        clsSize = 'col-md-' + size;

    return (
      <div className={clsSize}>
        {this.props.children}
      </div>
    );
  }

});

var Row = React.createClass({

  render: function() {
    return (
      <div className="row">
        {this.props.children}
      </div>
    );
  }

});

var ContainerFluid = React.createClass({

  render: function() {
    return (
      <div className="container-fluid">
        {this.props.children}
      </div>
    );
  }

});

var Label = React.createClass({

  render: function() {
    let type = this.props.type || 'default';
    let cls = `label label-${type}`;

    return (
      <span className={cls}>{this.props.text}</span>
    );
  }

});


var BlockButton = React.createClass({

  render: function() {
    return (
      <button type="button" className="btn btn-default btn-block" onClick={this.props.onClick}>
        {this.props.label}
      </button> 
    );
  }

});

var BtnGroup = React.createClass({

  render: function() {
    let buttons = _.map( this.props.buttons, (btn) => {
      return (
        <div className="btn-group" role="group">
          <button type="button" className="btn btn-default" onClick={btn.onClick}>
            {btn.label}
          </button>
        </div>
      );
    });

    return (
      <div className="btn-group btn-group-justified" role="group" aria-label="...">
        {buttons}
      </div> 
    );
  }

});

var InputGroup = React.createClass({

  getInitialState: function() {
    return {
      value: '1'
    };
  },

  render: function() {
    let buttons = [
      { label: 'Min', onClick: this.onMin },
      { label: '-5',  onClick: () => this.onChange(-5) },
      { label: '-1',  onClick: () => this.onChange(-1) },
      { label: '+1',  onClick: () => this.onChange(1) },
      { label: '+5',  onClick: () => this.onChange(5) },
      { label: 'Max', onClick: this.onMax }
    ];

    return (
      <div>
        <div className="input-group">
          <input type="text" className="form-control" placeholder="..." value={this.state.value} onChange={this.onValue} />
          <span className="input-group-btn">
            <button className="btn btn-default" type="button" onClick={this.onSubmit}>{this.props.button}</button>
          </span>
        </div>
        <BtnGroup buttons={buttons} />
      </div>
    );
  },

  onValue: function(e) {
    // allow only numbers and the empty string

    let val = e.target.value;
    if ( val.length === 0 ) {
      this.setState({ value: val });
    }
    else {
      if ( val.match(/^\d+$/) !== null )
        this.setState({ value: val });
    }
  },

  onMin: function() {
  },

  onMax: function() {
  },

  onChange: function( n ) {
    let val = parseInt( this.state.value, 10 );
    this.setState({ value: String(val + n) });
  },

  onSubmit: function() {
    let val = parseInt( this.state.value, 10 );
    this.props.onSubmit( val );
  }

});


var Panel = React.createClass({

  render: function() {
    let title = "";
    if ( this.props.title !== undefined ) {
      title = (
        <div className="panel-heading">
          <h3 className="panel-title">{this.props.title}</h3>
        </div>
      );
    }

    let clazz = "panel " + this.props.cls;

    return (
      <div className={clazz}>
        {title}
        <div className="panel-body">
          {this.props.children}
        </div>
      </div>
    );
  }

});




var TitleScreen = React.createClass({

  render: function() {
    return (
      <div className="text-center">
        <h1>The Silesian Challenge Game</h1>
        <p>Version 0.1</p>
        <h3>by Tomasz Primke</h3>
        <p>Inspired by The Opt Challenge Game</p>
      </div>
    );
  }

});

var ActionOnClick = {

  getInitialState: function() {
    let st = {
      active: false
    };

    if ( this.props.station )
      st.station = flux.getStore( this.props.station.store );

    if ( this.props.sink ) {
      st.sink = flux.getStore( 'market' );
    }

    return st;
  },

  componentWillMount: function() {
    flux.registerListener( this.onGui, 'gui' );
    if ( this.props.station )
      this.props.station.handler( this.onStation );
    if ( this.props.sink )
      game.onMarket( this.onMarket );
  },

  onClick: function() {
    if ( this.state.active === false ) {
      flux.queue( this.props.action );
      this.setState({ active: true });
    }
    else {
      flux.queue( 'gui-reset' );
      this.setState({ active: false });
    }
  },

  onGui: function(data) {
    if ( data.active === null )
      this.setState({ active: false });
    else
      if ( data.active !== this.props.active )
        this.setState({ active: false });
  },

  onStation: function(data) {
    this.setState({ station: data });
  },

  onMarket: function(data) {
    this.setState({ sink: data });
  },

  getClz: function() {
    let clz = "col-md-1";
    if ( this.state.active === true )
      clz += ' bg-primary';

    return clz;
  }

};

var Source = React.createClass({

  mixins: [ActionOnClick],

  render: function() {
    let clz = this.getClz();

    return (
      <div className={clz} onClick={this.onClick}>
        <div className="box">
          <p>{this.props.label}</p>
          <p>B {this.props.price}</p>
        </div>
      </div>
    );
  }

});

var WorkStation = React.createClass({

  mixins: [ActionOnClick],

  render: function() {
    let clz = this.getClz();
    var cls = "box " + this.props.resource;

    let station = this.state.station;
    if ( station === undefined ) // fallback, should be removed once all the stations are imlemented
      station = {
        state: '--idle--',
        buffer_in: 0,
        buffer_out: 0,
        timer: null
      };

    let rows = [
      (<tr>
         <td colspan="2">{station.state}</td>
       </tr>)
    ];

    if ( station.hasOwnProperty('buffer_in') ) {
      rows.push(
        <tr>
          <td>In:</td>
          <td><Label text={station.buffer_in} /></td>
        </tr>
      );
    }
    else {
      rows.push(
        <tr>
          <td>A:</td>
          <td><Label text={station.buffer_a} /></td>
        </tr>
      );
      rows.push(
        <tr>
          <td>D:</td>
          <td><Label text={station.buffer_d} /></td>
        </tr>
      );      
    }

    rows.push(
      <tr>
        <td>Out:</td>
        <td><Label text={station.buffer_out} /></td>
      </tr>
    );

    let time = (station.timer === null) ? '-' : station.timer;
    rows.push(
      <tr>
        <td>Time:</td>
        <td><Label text={time} /></td>
      </tr>
    );

    return (
      <div className={clz} onClick={this.onClick}>
        <div className={cls}>
          <p>{this.props.label}</p>
          <table width="100%" border="0">
            {rows}
          </table>
          <p>S: {this.props.setup} <br />P: {this.props.production}</p>
        </div>
      </div>
    );
  }

});

var Sink = React.createClass({

  mixins: [ActionOnClick],

  render: function() {
    let sink = this.state.sink;
    if ( sink === undefined ) // fallback, should be removed once all the sinks are imlemented
      sink = {
        main: 0,
        product_a: 0,
        product_b: 0
      };
    let products = 0;
    if ( this.props.sink )
      products = sink[ this.props.sink.product ];

    let clz = this.getClz();

    return (
      <div className={clz} onClick={this.onClick}>
        <div className="box">
          <p>{this.props.label}</p>
          <p>B {this.props.price}</p>
          <p>Sold: <Label text={products} /></p>
        </div>
      </div>
    );
  }

});

var ConnectionH = React.createClass({

  render: function() {
    var cls = "col-md-" + this.props.size;

    return (
      <div className={cls}>
        <div className="hline"></div>
      </div>
    );
  }

});

var ConnectionHD = React.createClass({

  render: function() {
    return (
      <div className="col-md-1">
        <div className="hline"></div>
        <div className="vdownline"></div>
      </div>
    );
  }

});

var ConnectionHU = React.createClass({

  render: function() {
    return (
      <div className="col-md-1">
        <div className="hline"></div>
        <div className="vupline"></div>
      </div>
    );
  }

});

var getProdComponent = function( spec ) {
  switch ( spec.type ) {
    
    case 'source': 
      return <Source label={spec.label} price={spec.price} action={spec.action} active={spec.active} />;
    
    case 'connection-h':
      return <ConnectionH size={spec.size} />;

    case 'connection-hd':
      return <ConnectionHD />;

    case 'connection-hu':
      return <ConnectionHU />;

    case 'spacer':
      return <ColMd size={spec.size} />;

    case 'workstation':
      return <WorkStation label={spec.label} resource={spec.resource} setup={spec.setup} production={spec.production} action={spec.action} active={spec.active} station={spec.station} />;

    case 'sink':
      return <Sink label={spec.label} price={spec.price} sink={spec.sink} action={spec.action} active={spec.active} />;

    default:
      return <p>Ooops</p>;
  }
};

var Line = React.createClass({

  render: function() {

    var components = _.map( this.props.spec, getProdComponent );

    return (
      <div className="row">
        {components}
      </div>
    );
  }

});

var SystemScreen = React.createClass({

  render: function() {

    var top_line = [
      { type: 'source', label: 'Raw material A', price: 10, action: 'gui-raw-a', active: 'raw-a' },
      { type: 'connection-h', size: 2 },
      { type: 'workstation', label: 'Workstation A', resource: 'res-1', setup: 240, production: 28, action: 'gui-sta-a', active: 'sta-a', station: { store: 'A', handler: game.onA } },
      { type: 'connection-h', size: 4 },
      { type: 'connection-hd' },
      { type: 'connection-h', size: 2 },
      { type: 'sink', label: 'Spare part A', price: 40, action: 'gui-sell-a', active: 'sell-a', sink: { product: 'product_a' } }
    ];

    var middle_line = [
      { type: 'spacer', size: 8 },
      { type: 'workstation', label: 'Workstation E', resource: 'res-3', setup: 180, production: 25, action: 'gui-sta-e', active: 'sta-e', station: { store: 'E', handler: game.onE } },
      { type: 'connection-h', size: 2 },
      { type: 'sink', label: 'Product', price: 60, sink: { product: 'main' } }
    ];

    var bottom_line = [
      { type: 'source', label: 'Raw material B', price: 10, action: 'gui-raw-b', active: 'raw-b' },
      { type: 'connection-h', size: 2 },
      { type: 'workstation', label: 'Workstation B', resource: 'res-2', setup: 120, production: 10, action: 'gui-sta-b', active: 'sta-b', station: { store: 'B', handler: game.onB } },
      { type: 'connection-h', size: 1 },
      { type: 'workstation', label: 'Workstation C', resource: 'res-1', setup: 360, production: 6, action: 'gui-sta-c', active: 'sta-c', station: { store: 'C', handler: game.onC } },
      { type: 'connection-h', size: 1 },
      { type: 'workstation', label: 'Workstation D', resource: 'res-2', setup: 360, production: 12, action: 'gui-sta-d', active: 'sta-d', station: { store: 'D', handler: game.onD } },
      { type: 'connection-hu' },
      { type: 'connection-h', size: 2 },
      { type: 'sink', label: 'Spare part D', price: 30, action: 'gui-sell-d', active: 'sell-d', sink: { product: 'product_d' } }
    ];

    return (
      <div className="container-fluid">
        <Line spec={top_line} />
        <Line spec={middle_line} />
        <Line spec={bottom_line} />
      </div>
    );
  }

});

var GameOverScreen = React.createClass({
  
  render: function() {
    return (
      <ContainerFluid>
        <div className="text-center">
          <h2>Game over</h2>
          <p>You messed everything up.</p>
          <p>People are going to lose their jobs.</p>
          <p>Their kids won't be happy.</p>
          <p>Are you satisfied?</p>
        </div>
      </ContainerFluid>
    );
  }

});

var InfoPanel = React.createClass({

  getInitialState: function() {
    return {
      cash:  flux.getStore('cash'),
      time:  flux.getStore('time'),
      state: 'stopped',
      delay: 1000
    };
  },

  componentWillMount: function() {
    flux.registerListener( this.onGameTime, "time" );
    flux.registerListener( this.onGameCash, "cash" );
  },

  render: function() {
    let { week, day, hour, minute } = this.getTime();
    let ctrl_label = (this.state.state === 'started') ? 'Stop' : 'Start';
    let buttons = [
      { label: 'Faster', onClick: () => this.onDelay(-100) },
      { label: 'Slower', onClick: () => this.onDelay(100) }
    ];

    return (
      <Panel title="Control panel" cls="panel-default">
        <div className="container">
          <Row>
            <ColMd size="6">
              <p>Cash: {this.state.cash}</p>
              <p>Week {week}, {day}, {hour}:{minute}</p>
            </ColMd>
            <ColMd size="6">
              <BlockButton label={ctrl_label} onClick={this.onControl} />
              <BtnGroup buttons={buttons} />
            </ColMd>
          </Row>
        </div>
      </Panel>
    );
  },

  onGameTime: function(time) {
    this.setState({time});
  },

  onGameCash: function(cash) {
    this.setState({cash});
  },

  getTime: function() {
    let minutes = this.state.time;

    const HOUR = 60,
          DAY  = HOUR * 8,
          WEEK = DAY * 5,
          DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

    let week = 1 + Math.floor( minutes / WEEK );
    minutes -= (week-1) * WEEK;

    let day = Math.floor( minutes / DAY );
    minutes -= day * DAY;
    day = DAYS[day];

    let hour = Math.floor( minutes / HOUR );
    minutes -= hour * HOUR;

    return { week, day, hour, minute: minutes };
  },

  onControl: function() {
    switch ( this.state.state) {
      case 'stopped':
        // start the game
        let time_id = setTimeout( this.onTime, this.state.delay );
        this.setState({state: 'started', time_id});
        break;

      case 'started':
        // stop the game
        clearTimeout( this.state.time_id );
        this.setState({state: 'stopped', time_id: null});
        break;
    }
  },

  onTime: function() {
    game.tick();
    let time_id = setTimeout( this.onTime, this.state.delay );
    this.setState({time_id});
  },

  onDelay: function(k) {
    if ( k < 0 ) {
      if ( this.state.delay < 200 )
        return;
    }
    else {
      if ( this.state.delay > 2000 )
        return;
    }

    this.setState({delay: this.state.delay + k});
  }

});

var DialogPanel = React.createClass({

  getInitialState: function() {
    return {
      gui: flux.getStore('gui')
    };
  },

  componentWillMount: function() {
    flux.registerListener( this.onGui, 'gui' );
  },

  render: function() {
    let clazz = "panel-primary";
    let title = this.state.gui.title;
    if ( title === null )
      clazz += ' invisible';

    // dialog
    let dialog = '';
    if ( this.state.gui.dialog !== null ) {
      let title = this.state.gui.dialog.title,
          btn_label = this.state.gui.dialog.button;

      dialog = (
        <div>
          <p>{title}</p>
          <InputGroup button={btn_label} onSubmit={this.onDialog} />
        </div>
      );
    }

    // actions
    let actions = _.map( this.state.gui.actions, (act) => {
      return (
        <BlockButton onClick={act.action} label={act.title} />
      );
    });

    let spacer = '';
    if ( (actions.length > 0) && (dialog !== '') )
      spacer = <hr />

    return (
      <Panel title={title} cls={clazz}>
        <div className="container">
          <Row>
            <ColMd size="6">
              {dialog}
            </ColMd>
            <ColMd size="6">
              {actions}
            </ColMd>
          </Row>
        </div>
      </Panel>
    );
  },

  onGui: function( data ) {
    this.setState({ gui: data });
  },

  onDialog: function( data ) {
    this.state.gui.dialog.func( data );
  }

});



var App = React.createClass({

  getInitialState: function() {
    let gui = flux.getStore('gui');
    return {
      screen: gui.screen
    };
  },

  componentDidMount: function() {
    game.onWeek( this.onWeek );
    flux.registerListener( (gui) => {
      if ( gui.screen !== 'game' ) {
        this.setState( { screen: gui.screen } );
      }
    }, 'gui' );
  },

  render: function() {
    let screen = '';
    if ( this.state.screen === 'game' )
      screen = <SystemScreen />;
    else
      screen = <GameOverScreen />;

    return (
      <div className="container-fluid fill">
        <div className="row fill">
          {screen}
          <InfoPanel />
          <DialogPanel />
        </div>
      </div>
    );
  },

  onWeek: function( info ) {
    if ( info.game_over === true ) {
      flux.queue( 'gui-gameover' );
    }
  }

});

ready( function() {
  game.init();

  flux.createStore({
    name: 'gui',
    init: {
      active:  null,
      dialog:  null,
      actions: null,
      title:   null,
      screen:  'game'
    },
    data: 'gui',
    onData: (callbacks, data) => {
      callbacks.setValue( data );
      callbacks.updated();
    }
  });

  flux.createAction({
    name: 'gui-reset',
    onRun: (callbacks) => {
      callbacks.data( 'gui', {
        active:  null,
        title:   null,
        dialog:  null,
        actions: null,
        screen:  'game'
      });
      callbacks.done();
    }
  });

  flux.createAction({
    name: 'gui-gameover',
    onRun: (callbacks) => {
      callbacks.data( 'gui', {
        active:  null,
        title:   null,
        dialog:  null,
        actions: null,
        screen:  'game-over'
      });
      callbacks.done();
    }
  });

  let createSellAction = (spec) => {
    let capital = spec.name.toUpperCase();

    flux.createAction({
      name: `gui-sell-${spec.name}`,
      onRun: (callbacks) => {
        callbacks.data( 'gui', {
          active: `sell-${spec.name}`,
          title:  `Sell product ${capital}`,
          dialog: {
            title:  `Sell product ${capital}`,
            button: 'Sell',
            func:   spec.func
          },
          actions: null,
          screen:  'game'
        });
        callbacks.done();
      }
    });
  };

  createSellAction({
    name: 'a',
    func: (num) => game.sell('A',num)
  });

  createSellAction({
    name: 'd',
    func: (num) => game.sell('D',num)
  });

  let createRawAction = (spec) => {
    let capital = spec.name.toUpperCase();

    flux.createAction({
      name: `gui-raw-${spec.name}`,
      onRun: (callbacks) => {
        callbacks.data( 'gui', {
          active:  `raw-${spec.name}`,
          title:   `Raw material ${capital}`,
          dialog:  {
            title:  `Buy raw material ${capital}`,
            button: 'Buy',
            func:   spec.func
          },
          actions: null,
          screen:  'game'
        });
        callbacks.done();
      }
    });
  };

  createRawAction({
    name: 'a',
    func: game.buyA
  });

  createRawAction({
    name: 'b',
    func: game.buyB
  });

  let createStationAction = (spec) => {
    let capital = spec.name.toUpperCase();
    let actions = [
      { title: 'Set up', action: () => game.setup(capital) }
    ];
    if ( spec.hasOwnProperty('actions') )
      actions = actions.concat( spec.actions );

    flux.createAction({
      name: `gui-sta-${spec.name}`,
      onRun: (callbacks) => {
        callbacks.data( 'gui', {
          active:  `sta-${spec.name}`,
          title:   `Workstation ${capital}`,
          dialog:  null,
          actions,
          screen:  'game'
        });
        callbacks.done();
      }
    });
  };

  let createStationTrAction = (spec) => {
    let capital = spec.name.toUpperCase();

    flux.createAction({
      name: `gui-sta-${spec.name}`,
      onRun: (callbacks) => {
        callbacks.data( 'gui', {
          active: `sta-${spec.name}`,
          title:  `Workstation ${capital}`,
          dialog: {
            title:  'Transport to E',
            button: 'Move',
            func:   spec.func
          },
          actions: [
            { title: 'Set up', action: () => game.setup(capital) }
          ],
          screen:  'game'
        });
        callbacks.done();
      }
    });
  };

  createStationTrAction({
    name: 'a',
    func: game.transAE
  });

  createStationAction({
    name: 'b'
  });

  createStationAction({
    name: 'c'
  });

  createStationTrAction({
    name: 'd',
    func: game.transDE
  });

  createStationAction({
    name: 'e'
  });

  
  React.render( <App />, document.getElementById('app') );
});


