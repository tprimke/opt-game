var jsn = require('jsn');

exports.createNet = function(spec) {

  // true, when the system went banckrupt and should work no more
  var stopped = false;

  // true, when the net has been initialised
  var initialised = false;

  // ----------------------------------------------------------------
  // Initial states for places
  // ----------------------------------------------------------------

  // the initial state for a simple workstation (1 input buffer, 1
  // output buffer)
  var initSimpleState = () => {
    return { state: 'idle', buffer_in: 0, buffer_out: 0, timer: null };
  };

  // the initial state for the assembling workstation (2 input
  // buffers, 1 output buffer)
  var initComplexState = () => {
    return { state: 'idle', buffer_a: 0, buffer_d: 0, buffer_out: 0, timer: null };
  };

  // the initial state for the market: no products at all
  var initMarket = () => {
    return { main: 0, product_a: 0, product_d: 0 };
  };

  // ----------------------------------------------------------------
  // Evaluators
  // ----------------------------------------------------------------

  // reduces the place token for the given price
  // used to pay for the raw materials
  var payRaw = (price) => {
    return {
      ready: (t) => t >= price,
      place: (t) => t - price
    };
  };

  // increases the amount of raw material in the input buffer
  var buyRaw = () => {
    return {
      ready: true,
      update: {
        place: (t) => {
          return {
            buffer_in: t.buffer_in + 1
          };
        }
      }
    };
  };

  // starts the setup process for the place
  var setupStart = (time) => {
    return {
      ready: true,
      update: {
        place: (t) => {
          return {
            timer: time,
            state: 'setup'
          };
        }
      }
    };
  };

  // stops any on-going process for the place
  // used to stop a workstation, when the resource has been taken to
  // an another workstation
  var setupStop = () => {
    return {
      ready: true,
      update: {
        place: (t) => {
          return {
            timer: null,
            state: 'idle'
          };
        }
      }
    };
  };

  // 
  var produceSimple = (time) => {
    return {
      ready: (t) => (t.state === 'ready') && (t.buffer_in > 0),
      update: {
        place: (t) => {
          return {
            timer:     time,
            buffer_in: t.buffer_in - 1,
            state:     'busy'
          };
        }
      }
    };
  };

  var produceComplex = (time) => {
    return {
      ready: (t) => (t.state === 'ready') && (t.buffer_a > 0) && (t.buffer_d > 0),
      update: {
        place: (t) => {
          return {
            timer:    time,
            buffer_a: t.buffer_a - 1,
            buffer_d: t.buffer_d - 1,
            state:    'busy'
          };
        }
      }
    };
  };

  var tick = () => {
    return {
      ready: (t) => (! stopped) && (t.timer > 0),
      update: {
        place: (t) => {
          if ( t.timer > 1 )
            return {
              timer: t.timer - 1
            };
          else {
            let val = {
              state:      'ready',
              timer:      null
            };
            
            if ( t.state === 'busy' ) // for production only
              val.buffer_out = t.buffer_out + 1;

            return val;
          }
        }
      }
    };
  };

  var onInit = () => {
    return {
      ready: (t) => (! initialised),
      place: (t) => {
        initialised = true;
        return true;
      },
      time:  2400
    };
  };

  var onWeekControl = () => {
    return {
      ready: (t) => (t === true) && (! stopped),
      place: (t) => t,
      time: 2400
    };
  };

  var onWeekCash = () => {
    return {
      ready: (t) => (! stopped),
      place: (t) => {
        if ( t >= 2500 ) {
          spec.afterWeek({
            game_over: false
          });
          return t - 2500;
        }
        else {
          stopped = true;
          spec.afterWeek({
            game_over: true
          });
          return 0;
        }
      }
    };
  };

  var transFrom = () => {
    return {
      ready: (t) => t.buffer_out > 0,
      update: {
        place: (t) => {
          return {
            buffer_out: t.buffer_out - 1
          };
        }
      }
    };
  };

  var transTo = (dest = 'buffer_in') => {
    return {
      ready: true,
      update: {
        place: (t) => {
          let val = {};
          val[dest] = t[dest] + 1;
          return val;
        }
      }
    };
  };

  var sellFrom = () => {
    return {
      ready: (t) => t.buffer_out > 0,
      update: {
        place: (t) => {
          return {
            buffer_out: t.buffer_out - 1
          };
        }
      }
    };
  };

  var sellTo = (dest = 'main') => {
    return {
      ready: (t) => {
        if ( dest === 'main' )
          return true;
        else
          return t.main > t[dest];
      },
      update: {
        place: (t) => {
          let val = {};
          val[dest] = t[dest] + 1;
          return val;
        }
      }
    };
  };

  var sellFor = (price) => {
    return {
      ready: (! stopped),
      place: (t) => t + price
    };
  };

  return jsn.Net({
    places: [
      { name: 'cash', token: 1500, onUpdate: spec.updateCash },

      { name: 'sta', token: initSimpleState(), onUpdate: spec.updateA },
      { name: 'stb', token: initSimpleState(), onUpdate: spec.updateB },
      { name: 'stc', token: initSimpleState(), onUpdate: spec.updateC },
      { name: 'std', token: initSimpleState(), onUpdate: spec.updateD },
      { name: 'ste', token: initComplexState(), onUpdate: spec.updateE },

      { name: 'market', token: initMarket(), onUpdate: spec.updateMarket },

      { name: 'control', token: false }
    ],

    transitions: [
      // the workstation A
      { name: 'buy-a' },
      { name: 'setup-a' },
      { name: 'tick-a', onTick: 'tick-a' },
      { name: 'produce-a', onReady: 'produce-a' },

      // transport A -> E
      { name: 'trans-ae' },
      
      // the workstation B
      { name: 'buy-b' },
      { name: 'setup-b' },
      { name: 'tick-b', onTick: 'tick-b' },
      { name: 'produce-b', onReady: 'produce-b' },

      // transport B -> C
      { name: 'trans-bc', onReady: 'trans-bc' },

      // the workstation C
      { name: 'setup-c' },
      { name: 'produce-c', onReady: 'produce-c' },
      { name: 'tick-c', onTick: 'tick-c' },

      // transport C -> D
      { name: 'trans-cd', onReady: 'trans-cd' },

      // the workstation D
      { name: 'setup-d' },
      { name: 'produce-d', onReady: 'produce-d' },
      { name: 'tick-d', onTick: 'tick-d' },

      // transport D -> E
      { name: 'trans-de' },

      // the workstation E
      { name: 'setup-e' },
      { name: 'produce-e', onReady: 'produce-e' },
      { name: 'tick-e', onTick: 'tick-e' },

      // sell the products
      { name: 'sell-e', onReady: 'sell-e' },
      { name: 'sell-a' },
      { name: 'sell-d' },

      // the simulation mechanics
      { name: 'init', onReady: 'init' },
      { name: 'onWeek', onReady: 'onWeek' }
    ],

    arcs: [
      // the workstation A
      { place: 'cash',   transition: 'buy-a',     evaluate: payRaw(10) },
      { place: 'sta',    transition: 'buy-a',     evaluate: buyRaw() },
      { place: 'sta',    transition: 'produce-a', evaluate: produceSimple(28) },
      { place: 'sta',    transition: 'tick-a',    evaluate: tick() },

      // sell the A product
      { place: 'sta',    transition: 'sell-a',    evaluate: sellFrom() },
      { place: 'market', transition: 'sell-a',    evaluate: sellTo('product_a') },
      { place: 'cash',   transition: 'sell-a',    evaluate: sellFor(40) },

      // transport A -> E
      { place: 'sta',    transition: 'trans-ae',  evaluate: transFrom() },
      { place: 'ste',    transition: 'trans-ae',  evaluate: transTo('buffer_a') },

      // the workstation B
      { place: 'cash',   transition: 'buy-b',     evaluate: payRaw(10) },
      { place: 'stb',    transition: 'buy-b',     evaluate: buyRaw() },
      { place: 'stb',    transition: 'produce-b', evaluate: produceSimple(10) },
      { place: 'stb',    transition: 'tick-b',    evaluate: tick() },

      // transport B -> C
      { place: 'stb',    transition: 'trans-bc',  evaluate: transFrom() },
      { place: 'stc',    transition: 'trans-bc',  evaluate: transTo() },

      // the workstation C
      { place: 'stc',    transition: 'produce-c', evaluate: produceSimple(6) },
      { place: 'stc',    transition: 'tick-c',    evaluate: tick() },
      
      // the workstations A and C use the same resource
      { place: 'sta',    transition: 'setup-a',   evaluate: setupStart(240) },
      { place: 'stc',    transition: 'setup-a',   evaluate: setupStop() },
      { place: 'sta',    transition: 'setup-c',   evaluate: setupStop() },
      { place: 'stc',    transition: 'setup-c',   evaluate: setupStart(360) },

      // transport C -> D
      { place: 'stc',    transition: 'trans-cd',  evaluate: transFrom() },
      { place: 'std',    transition: 'trans-cd',  evaluate: transTo() },

      // the workstations B and D use the same resource
      { place: 'stb',    transition: 'setup-b',   evaluate: setupStart(120) },
      { place: 'std',    transition: 'setup-b',   evaluate: setupStop() },
      { place: 'stb',    transition: 'setup-d',   evaluate: setupStop() },
      { place: 'std',    transition: 'setup-d',   evaluate: setupStart(360) },

      // the workstation D
      { place: 'std',    transition: 'produce-d', evaluate: produceSimple(12) },
      { place: 'std',    transition: 'tick-d',    evaluate: tick() },

      // sell the D product
      { place: 'std',    transition: 'sell-d',    evaluate: sellFrom() },
      { place: 'market', transition: 'sell-d',    evaluate: sellTo('product_d') },
      { place: 'cash',   transition: 'sell-d',    evaluate: sellFor(30) },

      // transport D -> E
      { place: 'std',    transition: 'trans-de',  evaluate: transFrom() },
      { place: 'ste',    transition: 'trans-de',  evaluate: transTo('buffer_d') },

      // the workstation E
      { place: 'ste',    transition: 'setup-e',   evaluate: setupStart(180) },
      { place: 'ste',    transition: 'produce-e', evaluate: produceComplex(25) },
      { place: 'ste',    transition: 'tick-e',    evaluate: tick() },

      // sell the main product
      { place: 'ste',    transition: 'sell-e',    evaluate: sellFrom() },
      { place: 'market', transition: 'sell-e',    evaluate: sellTo() },
      { place: 'cash',   transition: 'sell-e',    evaluate: sellFor(60) },

      // onWeek
      { place: 'control', transition: 'init',   evaluate: onInit() },
      { place: 'control', transition: 'onWeek', evaluate: onWeekControl() },
      { place: 'cash',    transition: 'onWeek', evaluate: onWeekCash() }

    ]
  });
};
