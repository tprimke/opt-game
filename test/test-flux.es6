var mocha = require('mocha'),
    should = require('should'),
    _      = require('lodash'),
    game   = require('../build/tmp/game');

describe( 'game', function() {

  beforeEach( function() {
    game.init();
  });

  describe( 'buy raw materials', function() {

    it( 'A', function() {

      var buf_in, the_cash;
      
      // register the listeners
      game.onA( (data) => {
        buf_in = data.buffer_in;
      });
      game.onCash( (cash) => {
        the_cash = cash;
      });

      // buy one piece
      game.buyA();

      // after some time, all the listeners should have been called
      buf_in.should.eql(1);
      the_cash.should.eql(1490);

    }); // A

    it( 'B', function() {

      var buf_in, the_cash;
      
      // register the listeners
      game.onB( (data) => {
        buf_in = data.buffer_in;
      });
      game.onCash( (cash) => {
        the_cash = cash;
      });

      // buy one piece
      game.buyB();

      // after some time, all the listeners should have been called
      buf_in.should.eql(1);
      the_cash.should.eql(1490);

    }); // B

    it( 'many pieces of A', function() {
      
      var buf_in, the_cash;
      
      // register the listeners
      game.onA( (data) => {
        buf_in = data.buffer_in;
      });
      game.onCash( (cash) => {
        the_cash = cash;
      });

      // buy ten pieces
      game.buyA(10);

      // after some time, all the listeners should have been called
      buf_in.should.eql(10);
      the_cash.should.eql(1400);
    }); // many pieces of A

    it( 'many pieces of B', function() {
      
      var buf_in, the_cash;
      
      // register the listeners
      game.onB( (data) => {
        buf_in = data.buffer_in;
      });
      game.onCash( (cash) => {
        the_cash = cash;
      });

      // buy ten pieces
      game.buyB(10);

      // after some time, all the listeners should have been called
      buf_in.should.eql(10);
      the_cash.should.eql(1400);
    }); // many pieces of B

  });

  describe( 'time', function() {

    it( 'tick', function(done) {

      // set up the time handler
      game.onTime( (time) => {
        time.should.eql(1); 
        done();
      });

      // tick the time
      game.tick();

    }); // tick

  }); // time

  describe( 'source workstations', function() {

    it( 'A works', function() {

      var buf_out;

      // register the listeners
      game.onA( (data) => {
        buf_out = data.buffer_out;
      });

      // buy one piece
      game.buyA();

      // setup the workstation
      game.setup('A');

      // wait until the production is ready
      for ( let i = 0; i < 268; i++ )
        game.tick();

      buf_out.should.eql(1);

    }); // A works

    it( 'B works', function() {

      var buf_out, buf_in;

      // register the listeners
      game.onB( (data) => {
        buf_out = data.buffer_out;
      });
      game.onC( (data) => {
        buf_in = data.buffer_in;
      });

      // buy one piece
      game.buyB();

      // setup the workstation
      game.setup('B');

      // wait until the production is ready
      for ( let i = 0; i < 130; i++ )
        game.tick();

      buf_out.should.eql(0);
      buf_in.should.eql(1);

    });

  }); // source workstations

  describe( 'line workstations', function() {
    
    it('C works', function() {
      
      var buf_out, buf_in;

      // register the listeners
      game.onC( (data) => {
        buf_out = data.buffer_out;
      });
      game.onD( (data) => {
        buf_in = data.buffer_in;
      });

      // buy one piece
      game.buyB();

      // setup the workstations
      game.setup('B');
      game.setup('C');

      // wait until the production is ready
      for ( let i = 0; i < 366; i++ )
        game.tick();

      buf_out.should.eql(0);
      buf_in.should.eql(1);

    });

    it('D works', function() {
      
      var buf_out;

      // register the listeners
      game.onD( (data) => {
        buf_out = data.buffer_out;
      });

      // buy one piece
      game.buyB();

      // setup the workstations
      game.setup('B');
      game.setup('C');

      // wait until the production is ready
      for ( let i = 0; i < 503; i++ ) {
        game.tick();

        if ( i === 130 )
          game.setup('D');
      }

      buf_out.should.eql(1);

    });
  }); // line workstations

  describe( 'main product', function() {
    
    it('E works', function() {
      
      var cash;

      // register the listeners
      game.onCash( (money) => {
        cash = money;
      });
      game.onC( (data) => {
        if ( data.buffer_in === 1 )
          game.setup('D');
      });
      game.onD( (data) => {
        if ( data.buffer_out === 1 )
          game.setup('A');
      });
      game.onA( (data) => {
        if ( data.buffer_out === 1 ) {
          game.transAE();
          game.transDE();
        }
      });

      // buy raw materials
      game.buyB();
      game.buyA();

      // setup the workstations
      game.setup('B');
      game.setup('C');
      game.setup('E');

      // wait until the production is ready
      for ( let i = 0; i < 1500/*661*/; i++ )
        game.tick();

      cash.should.eql(1480+60);

    });
  }); // main product

  describe( 'spare parts selling', function() {

    it('spare parts', function() {
      
      var part_a, part_d, main;

      // register the listeners
      game.onMarket( (data) => {
        part_a = data.product_a;
        part_d = data.product_d;
        main   = data.main;
      });

      // production control
      game.onB( (data) => {
        if ( (data.state === 'ready') && 
             (data.buffer_in === 0) &&
             (data.buffer_out === 0) )
          game.setup('D');
      });
      game.onC( (data) => {
        if ( (data.state === 'ready') && 
             (data.buffer_in === 0) &&
             (data.buffer_out === 0) )
          game.setup('A');
      });
      game.onA( (data) => {
        if ( data.buffer_out === 2 ) {
          game.transAE();
          game.transDE();
        }
      });

      // buy the raw materials
      game.buyA(2);
      game.buyB(2);

      // setup the workstations
      game.setup('B');
      game.setup('C');
      game.setup('E');

      // wait until the production is ready
      for ( let i = 0; i < 1500; i++ )
        game.tick();

      game.sell('A');
      game.sell('D');

      part_a.should.eql(1);
      part_d.should.eql(1);
      main.should.eql(1);

    });

    it( 'sell many parts', function() {

      var part_a, part_d, main;

      // register the listeners
      game.onMarket( (data) => {
        part_a = data.product_a;
        part_d = data.product_d;
        main   = data.main;
      });

      // production control
      game.onB( (data) => {
        if ( (data.state === 'ready') && 
             (data.buffer_in === 0) &&
             (data.buffer_out === 0) )
          game.setup('D');
      });
      game.onC( (data) => {
        if ( (data.state === 'ready') && 
             (data.buffer_in === 0) &&
             (data.buffer_out === 0) )
          game.setup('A');
      });
      game.onA( (data) => {
        if ( data.buffer_out === 6 ) {
          game.transAE(3);
          game.transDE(3);
        }
      });

      // buy the raw materials
      game.buyA(6);
      game.buyB(6);

      // setup the workstations
      game.setup('B');
      game.setup('C');
      game.setup('E');

      // wait until the production is ready
      for ( let i = 0; i < 1900; i++ )
        game.tick();

      game.sell('A', 3);
      game.sell('D', 3);

      part_a.should.eql(3);
      part_d.should.eql(3);
      main.should.eql(3);

    });
    
  }); // spare parts selling
  
  describe( 'weeks', function() {
    
    it('bankruptcy', function() {
      
      var game_over;

      // register the listeners
      game.onWeek( (data) => {
        game_over = data.game_over;
      });

      // don't do anything, just let the time pass...
      for ( let i = 0; i < 2500; i++ )
        game.tick();

      game_over.should.be.true;

    });

    it('success', function() {
      
      var game_over;

      // register the listeners
      game.onWeek( (data) => {
        game_over = data.game_over;
      });

      // production control
      let prod_a = 0,
          prod_d = 0,
          av_a = 0,
          av_d = 0;

      let prodDone = (data) =>
        (data.state === 'ready') &&
        (data.buffer_in === 0) &&
        (data.buffer_out === 0);

      game.onB( (data) => {
        if ( prodDone(data) ) {
          game.setup('D');
        }
      });

      game.onC( (data) => {
        if ( prodDone(data) ) {
          game.setup('A');
        }
      });

      game.onD( (data) => {
        if ( prod_d < 15 ) {
          if ( data.buffer_out > 0 ) {
            game.transDE();
            prod_d += 1;
          }
        }
        else {
          av_d = data.buffer_out;
        }
      });
      game.onA( (data) => {
        if ( prod_a < 15 ) {
          if ( data.buffer_out > 0 ) {
            game.transAE();
            prod_a += 1;
          }
        }
        else {
          if ( data.buffer_out > 0 ) {
            game.sell('A');
          }
        }
      });

      // prepare the lower line
      game.buyA(30);
      game.buyB(30);
      game.setup('B');
      game.setup('C');
      game.setup('E');

      // don't do anything, just let the time pass...
      for ( let i = 0; i < 2403; i++ ) {
        game.tick();
        if ( i === 2300 ) {
          game.sell('D', 15);
        }
      }

      game_over.should.be.false;

    });
  }); // weeks
  
  describe( 'workstation interrupts', function() {
    
    it('setup A breaks setup C', function() {
      
      var state = [], last;

      // register the listeners
      game.onC( (data) => {
        if ( last !== data.state ) {
          state.push( data.state );
          last = data.state;
        }
      });

      // start setting up the C workstation
      game.setup('C');

      // wait some time
      for ( let i = 0; i < 15; i++ )
        game.tick();

      // stop C setup by starting the A setup
      game.setup('A');

      state.length.should.eql(2);
      state[0].should.eql('setup');
      state[1].should.eql('idle');

    });

    it( 'setup A breaks production on C', function() {
      
      var c;

      // register the listeners
      game.onC( (data) => {
        c = data;
      });

      // setup the production
      game.buyB(10);
      game.setup('B');
      game.setup('C');

      // wait some time
      for ( let i = 0; i < 363; i++ )
        game.tick();

      // stop C setup by starting the A setup
      game.setup('A');

      c.state.should.eql('idle');
      c.buffer_in.should.eql(9);
      c.buffer_out.should.eql(0);

    });

    it( 'setup C breaks setup A', function() {

      var a;

      // register the listener
      game.onA( (data) => {
        a = data;
      });

      // setup the A workstation
      game.setup('A');

      // wait some time
      for ( let i = 0; i < 15; i++ )
        game.tick();

      // setup the C workstation
      game.setup('C');

      a.should.have.property('state', 'idle');
      
    });

    it( 'setup C breaks production on A', function() {

      var a;

      // register the listener
      game.onA( (data) => {
        a = data;
      });

      // setup the production
      game.buyA(10);
      game.setup('A');

      // wait some time
      for ( let i = 0; i < 250; i++ )
        game.tick();

      // setup the C workstation
      game.setup('C');

      a.should.have.property('state', 'idle');
      a.should.have.property('buffer_in', 9);
      a.should.have.property('buffer_out', 0);
      
    });

    it( 'setup B breaks setup D', function() {

      var d;

      // register the listener
      game.onD( (data) => {
        d = data;
      });

      // setup the D workstation
      game.setup('D');

      // wait some time
      for ( let i = 0; i < 15; i++ )
        game.tick();

      // setup the B workstation
      game.setup('B');

      d.should.have.property('state', 'idle');

    });

    it( 'setup B breaks production on D', function() {

      var d, moment = false, set = false, max = 1000;

      // register the listener
      game.onD( (data) => {
        d = data;
        if ( (data.state === 'busy') && (set === false) ) {
          moment = true;
          set = true;
        }
      });

      // production control
      game.onB( (data) => {
        if ( (data.state === 'ready') &&
             (data.buffer_in === 0) )
          game.setup('D');
      });

      // setup the production
      game.buyB(10);
      game.setup('B');
      game.setup('C');

      // wait some time
      for ( let i = 0; i < max; i++ ) {
        game.tick();
        if ( moment === true ) {
          max = i + 5;
          moment = false;
        }
      }

      // setup the B workstation
      game.setup('B');

      d.should.have.property('state', 'idle');
      d.should.have.property('buffer_in', 9);
      d.should.have.property('buffer_out', 0);
      
    });

    it( 'setup D breaks setup B', function() {

      var b;

      // register the listener
      game.onB( (data) => {
        b = data;
      });

      // setup the B workstation
      game.setup('B');

      // wait some time
      for ( let i = 0; i < 15; i++ )
        game.tick();

      // setup the D workstation
      game.setup('D');

      b.should.have.property('state', 'idle');

    });

    it( 'setup D breaks production on B', function() {

      var b;

      // register the listeners
      game.onB( (data) => {
        b = data;
      });

      // setup the production
      game.buyB(10);
      game.setup('B');

      // wait some time
      for ( let i = 0; i < 125; i++ )
        game.tick();

      // setup the D workstation
      game.setup('D');

      b.should.have.property('state', 'idle');
      b.should.have.property('buffer_in', 9);
      b.should.have.property('buffer_out', 0);
      
    });

  }); // workstation interrupts
  

});
