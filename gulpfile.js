var gulp       = require('gulp'),
    to5        = require('gulp-6to5'),
    del        = require('del'),
    vinylPaths = require('vinyl-paths'),
    rename     = require('gulp-rename'),
    mocha      = require('gulp-mocha'),
    browserify = require('gulp-browserify');

// Build JSN

gulp.task( 'clean', function() {
  return gulp.src( 'build/*' )
             .pipe( vinylPaths(del) );
});

gulp.task( 'build-html', ['clean'], function() {
  return gulp.src('src/html/*.html')
             .pipe( gulp.dest('build') );
});

gulp.task( 'build-css', ['clean'], function() {
  return gulp.src('src/css/*.css')
             .pipe( gulp.dest('build') );
});

gulp.task( 'build-es6', ['clean'], function() {
  return gulp.src('src/js/**/*.es6')
             .pipe(to5())
             .pipe(rename( function(path) {
               path.extname = '.js';
             }))
             .pipe( gulp.dest('build/tmp') );
});

gulp.task( 'build-opt', ['build-es6'], function() {
  return gulp.src('build/tmp/main.js')
             .pipe(browserify({
               insertGlobals: true
             }))
             .pipe(rename( function(path) {
               path.basename = 'opt';
             }))
             .pipe( gulp.dest('build') );
});

gulp.task( 'clean-after-build', ['build-opt'], function(cb) {
  del([
    'build/tmp*'
  ], cb);
});

// Build

gulp.task( 'build', ['build-html', 'build-css', 'build-opt', 'clean-after-build']);

// Test

gulp.task( 'build-test', ['build-es6'], function() {
  return gulp.src('test/test-*.es6')
             .pipe( to5() )
             .pipe(rename( function(path) {
               path.extname = '.js';
             }))
             .pipe( gulp.dest('test') );
});

gulp.task( 'test-cli', ['build-es6', 'build-test'], function() {
  return gulp.src(['test/test-*.js'], {read: false})
             .pipe(mocha({
               reporter: 'spec'
             }));
});

gulp.task( 'test', ['build-es6'], function() {
  console.log( 'lll' );
});
